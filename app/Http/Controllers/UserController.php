<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Models\RoleMenu;
use App\Models\UserMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
	/**
	 * Display a listing of the users
	 *
	 * @param  \App\Models\User  $model
	 * @return \Illuminate\View\View
	 */
	public function index(Request $request)
	{
		$limit = $request->input('limit') ?? 25;
		$filter = $request->input('filter') ?? null;

		try {
			if ($filter) {
				$data = User::where('roles', $filter)->paginate($limit);
			} else {
				$data = User::orderBy('id', 'asc')->paginate($limit);
			}

			$menu = RoleMenu::all();

			return view('pages.user', ['user' => $data, 'filter' => $filter, 'limit' => $limit, 'menu' => $menu]);
		} catch (\Throwable $th) {
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function user_menu($user_id)
	{
		try {
			$array = array();
			$count = array();
			$data = UserMenu::with('menu')
				->where('user_id', $user_id)->get();

			foreach ($data as $key => $value) {
				$count[] += $value->menu->id;
				$data[$key]->menu->flag = 1;
				array_push($array, $data[$key]->menu);
			}

			$menu = RoleMenu::whereNotIn('id', $count)->get();

			foreach ($menu as $key => $value) {
				$menu[$key]->flag = 0;
				array_push($array, $menu[$key]);
			}

			$array = json_encode($array);

			return $array;
		} catch (\Throwable $th) {
			throw $th;
		}
	}

	public function search(Request $request)
	{
		$keyword = trim($request->input('keyword'));
		$data = User::join('roles', 'users.roles', 'roles.id')
			->where('users.name', 'LIKE', '%' . $keyword . '%')
			->orWhere('users.email', 'LIKE', '%' . $keyword . '%')
			->orWhere('roles.name', 'LIKE', '%' . $keyword . '%')
			->paginate(5);

		foreach ($data as $key => $value) {
			if ($value) {
				$data[$key]->flag = $value->roles;
				$value->roles = Roles::where('id', $value->roles)->pluck('name')->first();
			}
		}

		$roles = Roles::all();
		$district = UserDistrict::all();

		$roles = Roles::all();
		$district = UserDistrict::all();

		return view('pages.user', ['user' => $data, 'roles' => $roles, 'districts' => $district, 'keyword' => $keyword]);
	}

	public function create(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'required|email|unique:users',
			'name' => 'required|string',
			'password' => 'required',
			'role' => 'required',
			'menu' => 'required'
		]);

		if ($validator->fails()) {
			return redirect()->back()->with('error', $validator->errors()->first());
		}

		DB::beginTransaction();

		try {
			$roles = $request->input('role');
			$menu = $request->input('menu');

			$user = User::create([
				'username' => $request->username,
				'name' => $request->name,
				'email' => $request->email,
				'password' => Hash::make($request->password),
				'roles' => $roles
			]);

			foreach ($menu as $key => $value) {
				UserMenu::create([
					'user_id' => $user->id,
					'role_menu' => $value
				]);
			}

			DB::commit();

			return redirect()->back()->with('status', 'User baru berhasil ditambahkan');
		} catch (\Throwable $th) {
			DB::rollBack();
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function update(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'up-id' => 'required|integer',
			'up-email' => 'required|email',
			'up-name' => 'required|string',
			'up-role' => 'required',
			'up-menu' => 'required',
		]);

		if ($validator->fails()) {
			return redirect()->back()->with('error', $validator->errors()->first());
		}

		DB::beginTransaction();

		try {
			$roles = $request->input('up-role');
			$menu = $request->input('up-menu');

			User::where('id', $request->input('up-id'))->update([
				'name' => $request->input('up-name'),
				'email' => $request->input('up-email'),
				'roles' => $roles
			]);

			UserMenu::where('user_id', $request->input('up-id'))
				->delete();
			foreach ($menu as $key => $value) {
				UserMenu::create([
					'user_id' => $request->input('up-id'),
					'role_menu' => $value
				]);
			}

			DB::commit();

			return redirect()->back()->with('status', 'Data user berhasil diupdate');
		} catch (\Throwable $th) {
			DB::rollBack();
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function destroy(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'id' => 'required|integer',
		]);

		if ($validator->fails()) {
			return redirect()->back()->with('error', $validator->errors()->first());
		}

		$id = $request->input('id');

		if (Auth::user()->roles != 'Admin') {
			return redirect()->back()->with('error', 'Hanya admin yang dapat melakukan aksi ini !');
		}

		DB::beginTransaction();

		try {
			UserMenu::where('user_id', $id)->delete();
			User::where('id', $id)->delete();

			DB::commit();
			return redirect()->back()->with('status', 'User berhasil di hapus');
		} catch (\Throwable $th) {
			DB::rollBack();
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function updatePassword(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'password' => 'required|string',
			'password_confirmation' => 'required_with:password|same:password'
		]);

		if ($validator->fails()) {
			return redirect()->back()->with('error', $validator->errors()->first());
		}

		$password = $request->input('password');
		if (Hash::check($password, Auth::user()->password)) {
			return redirect()->back()->with('error', 'Password tidak boleh sama dengan sebelumnya');
		}

		DB::beginTransaction();
		try {
			User::where('id', Auth::user()->id)->update([
				'password' => Hash::make($password)
			]);

			DB::commit();
			return redirect()->back()->with('status', 'Password berhasil dirubah');
		} catch (\Throwable $th) {
			DB::rollBack();
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function login(Request $request)
	{
		$this->validate($request, [
			'login'    => 'required',
			'password' => 'required',
		]);

		$login_type = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL)
			? 'email'
			: 'username';

		$request->merge([
			$login_type => $request->input('login')
		]);

		if (Auth::attempt($request->only($login_type, 'password'))) {
			return redirect('home');
		}

		return redirect()->back()
			->with('error', 'Username / Email dan Password tidak sesuai');
	}
}
