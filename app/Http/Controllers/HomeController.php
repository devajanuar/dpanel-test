<?php

namespace App\Http\Controllers;

use App\Models\TodoList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
		$id = Auth::user()->id;

		//Tidak menggunakan find karena sering terjadi error
		try {
			$data = TodoList::where('user_id', $id)->get();
			$count = [
				'total' => count($data)
			];

			return view('dashboard', ['data' => $data, 'count' => $count]);
		} catch (\Throwable $th) {
			return view('dashboard')->with('error', $th->getMessage());
		}
    }

	public function addTodo(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'title' => 'required|string',
			'todo' => 'required|string'
		]);

		if ($validator->fails()) {
			return redirect()->back()->with('error', $validator->errors()->first());
		}

		$title = $request->input('title');
		$todo = $request->input('todo');

		try {
			TodoList::create([
				'user_id' => Auth::user()->id,
				'title' => $title,
				'list' => $todo
			]);

			return redirect()->back()->with('status', 'To Do List berhasil ditambahkan');
		} catch (\Throwable $th) {
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function editTodo(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'id' => 'required|integer',
			'title' => 'required|string',
			'todo' => 'required|string'
		]);

		if ($validator->fails()) {
			return redirect()->back()->with('error', $validator->errors()->first());
		}

		$id = $request->input('id');
		$title = $request->input('title');
		$todo = $request->input('todo');

		try {
			TodoList::where('id', $id)->update([
				'title' => $title,
				'list' => $todo
			]);

			return redirect()->back()->with('status', 'To Do List berhasil diupdate');
		} catch (\Throwable $th) {
			return redirect()->back()->with('error', $th->getMessage());
		}
	}

	public function destroyTodo(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'id' => 'required|integer'
		]);

		if ($validator->fails()) {
			return redirect()->back()->with('error', $validator->errors()->first());
		}

		$id = $request->input('id');

		try {
			TodoList::where('id', $id)->delete();

			return redirect()->back()->with('status', 'ToDoList berhasil di hapus');
		} catch (\Throwable $th) {
			return redirect()->back()->with('error', $th->getMessage());
		}
	}
}
