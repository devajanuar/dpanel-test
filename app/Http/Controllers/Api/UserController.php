<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
	public function login(Request $request)
	{
		$this->validate($request, [
			'email'    => 'required',
			'password' => 'required',
		]);


		if (!Auth::attempt($request->only('email', 'password'))) {
			return $this->error('Unauthorized');
		} else {
			$user = $request->user();
			$tokenResult = $user->createToken('Personal Access Token');
			$token = $tokenResult->plainTextToken;
			if ($request->remember_me) {
				$tokenResult->expires_at = Carbon::now()->addHour(10)->toDateTimeString();
			} else {
				$tokenResult->expires_at = null;
			}

			$data = [
				'userData' => $user,
				'accessToken' => $token,
				'token_type' => 'Bearer',
				'expires_at' => $tokenResult->expires_at,
				'authenticated' => true
			];
			return $this->success($data, 'Success');
		}
	}
}
