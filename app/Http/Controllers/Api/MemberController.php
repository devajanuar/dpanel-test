<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MemberController extends Controller
{
    public function member_by_id(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'member_id' => 'required|string',
		]);

		if ($validator->fails()) {
			return $this->error($validator->errors()->first());
		}

		$id = $request->input('member_id');

		try {
			$data = Member::with('group')
			->where('member_id', $id)->first();

			return $this->success($data, 'Success');
		} catch (\Throwable $th) {
			return $this->error($th->getMessage());
		}
	}
}
