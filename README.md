## Getting started

## Installation
Run composer install

Create .env file copy from .env.example

Run php artisan key:generate

Set up environment for database: 
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=webdevforus
DB_USERNAME=root
DB_PASSWORD=
```

Migration
```
php artisan migrate --seed
```

Run php artisan serve

## Requirement

Composer version 2.0.11

PHP version 7.4.28

Laravel version 8

## Dummy User
admin@argon.com

secret

## Documentation
https://documenter.getpostman.com/view/11341618/UzXNSwJB

## Changelog v 1.0.0
- Initial Commit
