<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_menus')->insert([
			['id' => '1', 'name' => 'Dashboard', 'url' => 'home'],
			['id' => '2', 'name' => 'User Management', 'url' => 'user'],
			['id' => '3', 'name' => 'Group Management', 'url' => 'group'],
			['id' => '4', 'name' => 'Member Management', 'url' => 'member']
		]);

		DB::table('user_menus')->insert([
			['user_id' => '1', 'role_menu' => 1],
			['user_id' => '1', 'role_menu' => 2]
		]);
    }
}
