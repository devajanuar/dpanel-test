<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TodoSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('todo_lists')->insert([
			'user_id' => 1,
			'title' => 'Test Case',
			'list' => "<ul><li>Tes 1</li><li>Tes 2</li></ul>",
		]);
	}
}
