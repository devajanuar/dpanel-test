@extends('layouts.app')

@section('content')
<div class="container-fluid mt-3">
	<div class="row mb-5">
		<div class="col">
			<h1>Member Management</h1>
		</div>
	</div>
	<!-- <form class="row" action="{{ url('/user-search') }}" method="GET">
		<div class="col">
			<div class="form-group">
				<div class="input-group mb-4">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fa fa-search"></i></span>
					</div>
					<input class="form-control" value="{{isset($keyword) ? $keyword : ''}}" placeholder="Search" name="keyword" type="text">
				</div>
			</div>
		</div>
		<div class="mr-2">
			@if (isset($filter))
			<a href="{{url('/user')}}" class="btn btn-danger">Reset Filter</a>
			@endif
			<button type="submit" data-toggle="dropdown" class="btn btn-success">Filter</button>
			<div class="dropdown-menu dropdown-menu-right mt-2" aria-labelledby="print-toggle">
				<a class="dropdown-item" href="{{url('/user')}}">Semua User</a>
				<a class="dropdown-item" href="{{url('/user?filter=2')}}">Desa</a>
				<a class="dropdown-item" href="{{url('/user?filter=3')}}">Kecamatan</a>
				<a class="dropdown-item" href="{{url('/user?filter=4')}}">Staff</a>
				<a class="dropdown-item" href="{{url('/user?filter=5')}}">Kasi</a>
				<a class="dropdown-item" href="{{url('/user?filter=6')}}">Kabid</a>
			</div>
		</div>
		<div class="mr-3">
			<button type="submit" class="btn btn-primary">Search</button>
		</div>
	</form> -->
	@if (session()->has('status'))
	<div class="alert alert-success alert-dismissible fade show" role="alert">
		<span class="alert-inner--text"><strong>Sukses! </strong> {{ session()->get('status') }}</span>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	@endif
	@if (session()->has('error'))
	<div class="alert alert-danger alert-dismissible fade show" role="alert">
		<span class="alert-inner--text"><strong>Error! </strong> {{ session()->get('error') }}</span>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	@endif

	<div class="row">
		<div class="col">
			<div class="card">
				<!-- Card header -->
				<div class="card-header border-0">
					<div class="row">
						<div class="col form-inline">
							<label>
								Show
								<form action="{{route('member')}}" method="GET">
									<select onchange="this.form.submit()" class="form-custom-select" id="limit" name="limit">
										@if (isset($limit))
										<option value="{{$limit}}" disabled selected>{{$limit}}</option>
										@endif
										<option value="25">25</option>
										<option value="50">50</option>
										<option value="100">100</option>
									</select>
								</form>
								Entries
							</label>
						</div>
						<div class="col-4 text-right">
							<a href="#" data-toggle="modal" data-target="#modal-default" class="btn btn-sm btn-primary">Add Member</a>
							<a href="#" data-toggle="modal" data-target="#modal-import" class="btn btn-sm btn-primary">Import Member</a>
						</div>
					</div>
				</div>
				<!-- Light table -->
				<div class="table-responsive">
					<table class="table align-items-center table-flush">
						<thead class="thead-light">
							<tr>
								<th scope="col" class="sort" data-sort="name">Member ID</th>
								<th scope="col" class="sort" data-sort="status">Group</th>
								<th scope="col" class="sort" data-sort="status">Nama</th>
								<th scope="col" class="sort" data-sort="status">Alamat</th>
								<th scope="col" class="sort" data-sort="status">No HP</th>
								<th scope="col" class="sort" data-sort="status">Email</th>
								<th scope="col" class="sort" data-sort="status">Profile Picture</th>
								<th scope="col">Aksi</th>
							</tr>
						</thead>
						<tbody class="list">
							@foreach ($member as $key => $brand)
							<tr>
								<td>{{ $brand->member_id }}</td>
								<td>{{ substr($brand->group->namagroup, 0, 50) }}</td>
								<td>{{ substr($brand->nama, 0, 50) }}</td>
								<td>{{ substr($brand->alamat, 0, 50) }}</td>
								<td>{{ substr($brand->hp, 0, 50) }}</td>
								<td>{{ substr($brand->email, 0, 50) }}</td>
								<td>
									<span class="avatar avatar-sm rounded-circle">
										<img alt="Image placeholder" src="{{$brand->profile_pic}}">
									</span>
								</td>
								<td>
									<div class="form-inline">
										<button type="button" class="btn btn-sm btn-primary" data-target="#modalAdd{{$brand->id}}" data-toggle="modal" onclick="enableUp('{{$brand->flag}}')">Edit</button>
										<form action="{{ route('member-destroy') }}" method="POST">
											@csrf
											<input type="text" hidden value="{{$brand->id}}" id="id" name="id">
											<button type="submit" class="btn btn-sm btn-danger">Delete</button>
										</form>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- Card footer -->
				<div class="card-footer py-4">
					<nav aria-label="...">
						<ul class="pagination justify-content-end mb-0">
							{!! $member->links() !!}
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>

	{{-- modal add --}}
	<div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
		<div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
			<div class="modal-content">

				<div class="modal-header">
					<h3 class="modal-title" id="modal-title-default">Tambah Member</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<form method="POST" action="{{ route('member-create') }}" enctype="multipart/form-data">
					@csrf
					<div class="modal-body">
						<div class="mb-3">
							<label for="name" class="form-label">Nama</label>
							<input type="text" name="name" class="form-control" id="name" placeholder="Nama">
						</div>
						<div class="mb-3">
							<label for="groupid">Group</label>
							<select class="form-control" name="groupid" id="groupid">
								<option selected disabled>::Pilih Group::</option>
								@foreach ($group as $groups)
								<option value="{{$groups->id}}">{{$groups->namagroup}}</option>
								@endforeach
							</select>
						</div>
						<div class="mb-3">
							<label for="alamat" class="form-label">Alamat</label>
							<input type="textarea" name="alamat" class="form-control" id="alamat" placeholder="Perum Puri">
						</div>
						<div class="mb-3">
							<label for="hp" class="form-label">No HP</label>
							<input type="text" name="hp" class="form-control" id="hp" placeholder="08xxx">
						</div>
						<div class="mb-3">
							<label for="email" class="form-label">Email</label>
							<input type="email" name="email" class="form-control" id="email" placeholder="deva@argon.com">
						</div>
						<div class="mb-3">
							<label for="picture" class="form-label">Profile Picture</label>
							<input type="file" accept="image/png, image/gif, image/jpeg" name="picture" class="form-control" id="picture">
							<p><i>Gambar tidak bisa lebih besar dari 2MB</i></p>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline-primary" class="close" data-dismiss="modal" aria-label="Close">Batal</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<br />
	{{-- modal edit --}}
	@foreach ($member as $brand)
	<div class="modal fade" id="modalAdd{{ $brand->id }}" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
		<div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
			<div class="modal-content">

				<div class="modal-header">
					<h3 class="modal-title" id="modal-title-default">Update Group</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<form method="POST" action="{{ route('member-update') }}" enctype="multipart/form-data">
					@csrf
					<div class="modal-body">
						<div class="mb-3">
							<input hidden type="text" name="up-id" class="form-control" id="up-id" value="{{$brand->member_id}}">
							<label for="up-name" class="form-label">Nama</label>
							<input type="text" name="up-name" class="form-control" id="up-name" value="{{$brand->nama}}" placeholder="Nama">
						</div>
						<div class="mb-3">
							<label for="up-groupid">Group</label>
							<select class="form-control" name="up-groupid" id="up-groupid">
								<option selected value="{{$brand->groupid}}">{{$brand->group->namagroup}}</option>
								@foreach ($group as $groups)
								<option value="{{$groups->id}}">{{$groups->namagroup}}</option>
								@endforeach
							</select>
						</div>
						<div class="mb-3">
							<label for="up-alamat" class="form-label">Alamat</label>
							<input type="textarea" name="up-alamat" class="form-control" id="up-alamat" value="{{$brand->alamat}}" placeholder="Perum Puri">
						</div>
						<div class="mb-3">
							<label for="up-hp" class="form-label">No HP</label>
							<input type="text" name="up-hp" class="form-control" id="up-hp" value="{{$brand->hp}}" placeholder="08xxx">
						</div>
						<div class="mb-3">
							<label for="up-email" class="form-label">Email</label>
							<input type="email" name="up-email" class="form-control" id="up-email" value="{{$brand->email}}" placeholder="deva@argon.com">
						</div>
						<div class="mb-3">
							<label for="up-picture" class="form-label">Profile Picture</label>
							<input type="file" accept="image/png, image/gif, image/jpeg" name="up-picture" class="form-control" id="up-picture">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline-primary" class="close" data-dismiss="modal" aria-label="Close">Batal</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	@endforeach

	{{-- modal import --}}
	<div class="modal fade" id="modal-import" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
		<div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
			<div class="modal-content">

				<div class="modal-header">
					<h3 class="modal-title" id="modal-title-default">Import Data Member</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<form method="POST" action="{{ route('member-import') }}" enctype="multipart/form-data">
					@csrf
					<div class="modal-body">
						<div class="mb-3">
							<label for="file" class="form-label">Excel Import</label>
							<input type="file" accept=".csv" name="file" class="form-control" id="file">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline-primary" class="close" data-dismiss="modal" aria-label="Close">Batal</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	@include('layouts.footers.auth')
</div>
@endsection