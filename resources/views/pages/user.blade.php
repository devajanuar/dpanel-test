@extends('layouts.app')

@section('content')
<div class="container-fluid mt-3">
	<div class="row mb-5">
		<div class="col">
			<h1>Manajemen User</h1>
		</div>
	</div>
	<!-- <form class="row" action="{{ url('/user-search') }}" method="GET">
		<div class="col">
			<div class="form-group">
				<div class="input-group mb-4">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fa fa-search"></i></span>
					</div>
					<input class="form-control" value="{{isset($keyword) ? $keyword : ''}}" placeholder="Search" name="keyword" type="text">
				</div>
			</div>
		</div>
		<div class="mr-2">
			@if (isset($filter))
			<a href="{{url('/user')}}" class="btn btn-danger">Reset Filter</a>
			@endif
			<button type="submit" data-toggle="dropdown" class="btn btn-success">Filter</button>
			<div class="dropdown-menu dropdown-menu-right mt-2" aria-labelledby="print-toggle">
				<a class="dropdown-item" href="{{url('/user')}}">Semua User</a>
				<a class="dropdown-item" href="{{url('/user?filter=2')}}">Desa</a>
				<a class="dropdown-item" href="{{url('/user?filter=3')}}">Kecamatan</a>
				<a class="dropdown-item" href="{{url('/user?filter=4')}}">Staff</a>
				<a class="dropdown-item" href="{{url('/user?filter=5')}}">Kasi</a>
				<a class="dropdown-item" href="{{url('/user?filter=6')}}">Kabid</a>
			</div>
		</div>
		<div class="mr-3">
			<button type="submit" class="btn btn-primary">Search</button>
		</div>
	</form> -->
	@if (session()->has('status'))
	<div class="alert alert-success alert-dismissible fade show" role="alert">
		<span class="alert-inner--text"><strong>Sukses! </strong> {{ session()->get('status') }}</span>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	@endif
	@if (session()->has('error'))
	<div class="alert alert-danger alert-dismissible fade show" role="alert">
		<span class="alert-inner--text"><strong>Error! </strong> {{ session()->get('error') }}</span>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	@endif

	<div class="row">
		<div class="col">
			<div class="card">
				<!-- Card header -->
				<div class="card-header border-0">
					<div class="row">
						<div class="col form-inline">
							<label>
								Show
								<form action="{{route('user')}}" method="GET">
									<select onchange="this.form.submit()" class="form-custom-select" id="limit" name="limit">
										@if (isset($limit))
										<option value="{{$limit}}" disabled selected>{{$limit}}</option>
										@endif
										<option value="5">5</option>
										<option value="10">10</option>
										<option value="25">25</option>
										<option value="50">50</option>
										<option value="100">100</option>
									</select>
								</form>
								Entries
							</label>
						</div>
						<div class="col-4 text-right">
							<a href="#" data-toggle="modal" data-target="#modal-default" class="btn btn-sm btn-primary">Add user</a>
						</div>
					</div>
				</div>
				<!-- Light table -->
				<div class="table-responsive">
					<table class="table align-items-center table-flush">
						<thead class="thead-light">
							<tr>
								<th scope="col" class="sort" data-sort="name">ID</th>
								<th scope="col" class="sort" data-sort="status">Nama</th>
								<th scope="col" class="sort" data-sort="status">Email</th>
								<th scope="col" class="sort" data-sort="status">Role User</th>
								<th scope="col">Aksi</th>
							</tr>
						</thead>
						<tbody class="list">
							@foreach ($user as $key => $brand)
							<tr>
								<td>{{ $brand->id }}</td>
								<td>{{ substr($brand->name, 0, 50) }}</td>
								<td>{{ substr($brand->email, 0, 50) }}</td>
								<td>{{ substr($brand->roles, 0, 50) }}</td>
								<td>
									<div class="form-inline">
										<button type="button" class="btn btn-sm btn-primary" data-target="#modalAdd{{$brand->id}}" data-toggle="modal" onclick="enableUp('{{$brand->flag}}')">Edit</button>
										<form action="{{ route('user-destroy') }}" method="POST">
											@csrf
											<input type="text" hidden value="{{$brand->id}}" id="id" name="id">
											<input type="text" hidden value="{{$brand->flag}}" id="flag" name="flag">
											<button type="submit" class="btn btn-sm btn-danger">Delete</button>
										</form>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- Card footer -->
				<div class="card-footer py-4">
					<nav aria-label="...">
						<ul class="pagination justify-content-end mb-0">
							{!! $user->links() !!}
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>

	{{-- modal photos --}}
	@foreach ($user as $brand)
	<div class="modal fade" id="modalEdit{{ $brand->id }}" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true">
		<div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
			<div class="modal-content">

				<div class="modal-header">
					<h3 class="modal-title" id="modal-title-default">Foto Rumah</h3>
					<a href="{{ route('user') }}">
						<span aria-hidden="true">×</span>
					</a>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="row">
									@if ($brand->photos)
									<button class="btn btn-sm">
									</button>
									@foreach ($brand->photos as $image)
									<a onclick="handleClickImage('{{$image}}')">
										<img src="{{$image}}" class="mx-1" style="width: 30px; height: 30px; background-color:salmon">
									</a>
									@endforeach
									<button class="btn btn-sm ml-1">></button>
									@endif
								</div>
								<div class="row">
									<div class="mx-2">
										@if ($brand->photos)
										<img src="{{$brand->photos[0]}}" id="image_thumbnail" style="width: 20rem; height: 15rem">
										@else
										<img src="https://www.btklsby.go.id/images/placeholder/basic.png" style="width: 12.5rem; height: 12.5rem">
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endforeach
	{{-- modal add --}}
	<div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
		<div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
			<div class="modal-content">

				<div class="modal-header">
					<h3 class="modal-title" id="modal-title-default">Tambah User</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<form method="POST" action="{{ route('user-create') }}">
					@csrf
					<div class="modal-body">
						<div class="mb-3">
							<label for="name" class="form-label">Nama</label>
							<input type="text" name="name" class="form-control" id="name" placeholder="Nama">
						</div>
						<div class="mb-3">
							<label for="email" class="form-label">Email</label>
							<input type="text" name="email" class="form-control" id="email" placeholder="Email">
						</div>
						<div class="mb-3">
							<label for="password" class="form-label">Password</label>
							<input type="password" name="password" class="form-control" id="name" placeholder="Password">
						</div>
						<div class="mb-3">
							<label for="role" class="form-label">Role Posisi</label>
							<input type="text" class="form-control" name="role" id="role" />
						</div>
						<div class="mb-3">
							<label class="form-label">Role Menu</label>
						</div>
						@foreach($menu as $item)
						<div>
							<input type="checkbox" id="menu{{$item->id}}" name="menu[]" value="{{$item->id}}">{{$item->name}}
						</div>
						@endforeach
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline-primary" class="close" data-dismiss="modal" aria-label="Close">Batal</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<br />
	{{-- modal edit --}}
	@foreach ($user as $brand)
	<div class="modal fade" id="modalAdd{{ $brand->id }}" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
		<div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
			<div class="modal-content">

				<div class="modal-header">
					<h3 class="modal-title" id="modal-title-default">Update User</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<form action="{{ route('user-update') }}" method="POST">
					@csrf
					<div class="modal-body">
						<div class="mb-3">
							<input hidden type="text" name="up-id" class="form-control" id="up-id" value="{{$brand->id}}">
							<input hidden type="text" name="up-roles" class="form-control" id="up-role" value="{{$brand->flag}}">
						</div>
						<div class="mb-3">
							<label for="up-name" class="form-label">Nama</label>
							<input type="text" name="up-name" class="form-control" id="up-name" value="{{$brand->name}}" placeholder="Nama">
						</div>
						<div class="mb-3">
							<label for="up-email" class="form-label">Email</label>
							<input type="text" name="up-email" class="form-control" id="up-email" value="{{$brand->email}}" placeholder="Email">
						</div>
						<div class="mb-3">
							<label for="exampleFormControlSelect1">Role Posisi</label>
							<input type="text" class="form-control" name="up-role" id="up-role" value="{{$brand->roles}}" />
						</div>
						<div class="mb-3">
							<label class="form-label">Role Menu</label>
						</div>
						@foreach(json_decode(App\Http\Controllers\UserController::user_menu($brand->id)) as $item)
						<div>
							@if($item->flag == 1)
							<input type="checkbox" id="up-menu{{$item->id}}" name="up-menu[]" value="{{$item->id}}" checked>{{$item->name}}
							@else
							<input type="checkbox" id="up-menu{{$item->id}}" name="up-menu[]" value="{{$item->id}}">{{$item->name}}
							@endif
						</div>
						@endforeach
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline-primary" class="close" data-dismiss="modal" aria-label="Close">Batal</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	@endforeach
	@include('layouts.footers.auth')
</div>
@endsection