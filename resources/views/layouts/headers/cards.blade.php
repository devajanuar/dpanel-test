<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
	<div class="container-fluid">
		<div class="header-body">
			<!-- Card stats -->
			<div class="row" style="justify-content: center;">
				<div class="col-xl-3 col-lg-6">
					<div class="card card-stats mb-4 mb-xl-0">
						<div class="card-body">
							<div class="row">
								<div class="col">
									<h5 class="card-title text-uppercase text-muted mb-0">Todo List</h5>
									<span class="h2 font-weight-bold mb-0">{{$count['total']}}</span>
								</div>
								<div class="col-auto">
									<a class="icon icon-shape bg-success text-white rounded-circle shadow" data-toggle="modal" data-target="#modalAdd">
										<i class="fas fa-plus"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
			<div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title" id="modal-title-default">Tambah Todo</h3>
						<a href="{{url('home')}}">
							<span aria-hidden="true">×</span>
						</a>
					</div>
					<div class="modal-body">
						<div class="container">
							<form action="{{url('/add-todo')}}" method="POST" enctype="multipart/form-data">
								@csrf
								<div class="modal-body">
									<div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
										<label class="form-control-label" for="input-title">{{ __('Judul') }}</label>
										<input type="text" name="title" id="input-title" class="form-control form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}" placeholder="{{ __('Judul') }}" required autofocus>

										@if ($errors->has('title'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('title') }}</strong>
										</span>
										@endif
									</div>
									<div class="form-group{{ $errors->has('todo') ? ' has-danger' : '' }}">
										<label class="form-control-label" for="todo">{{ __('Todo Task') }}</label>
										<textarea id="todo" name="todo"></textarea>
										@if ($errors->has('todo'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('todo') }}</strong>
										</span>
										@endif
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-secondary">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$('#todo').summernote({
		placeholder: 'To Do List',
		tabsize: 2,
		height: 120,
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'underline', 'clear']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']]
		]
	});
</script>