<div class="row align-items-center justify-content-xl-between">
    <div class="col-xl-6">
        <div class="copyright text-center text-xl-left text-muted">
            &copy; {{ now()->year }} <a href="https://www.linkedin.com/in/deva-januar-arifandi-a89173177/" class="font-weight-bold ml-1" target="_blank">Deva Januar Arifandi</a>
        </div>
    </div>
</div>