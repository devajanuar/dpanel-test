@extends('layouts.app')

@section('content')
@include('layouts.headers.cards', $count)

<div class="container-fluid mt--7">
	@if (session()->has('status'))
	<div class="alert alert-success alert-dismissible fade show" role="alert">
		<span class="alert-inner--text"><strong>Sukses! </strong> {{ session()->get('status') }}</span>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	@endif
	@if (session()->has('error'))
	<div class="alert alert-danger alert-dismissible fade show" role="alert">
		<span class="alert-inner--text"><strong>Error! </strong> {{ session()->get('error') }}</span>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	@endif
	@if($count['total'] > 0)
	<div class="row mt-5">
		@foreach($data as $key => $item)
		<div class="col-xl-4">
			<div class="card shadow">
				<div class="card-header border-0">
					<div class="row align-items-center">
						<div class="col">
							<h3 class="mb-0">{{$item->title}}</h3>
						</div>
						<div class="col text-right">
							<a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modalDelete{{ $item->id }}">X</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					{!! substr($item->list,0,200) !!}
				</div>
				<div class="card-footer text-right">
					<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalEdit{{ $item->id }}">Edit</a>
				</div>
			</div>
		</div>
		<!-- Modal Edit -->
		<div class="modal fade" id="modalEdit{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true">
			<div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title" id="modal-title-default">Edit Todo</h3>
						<a href="{{url('home')}}">
							<span aria-hidden="true">×</span>
						</a>
					</div>
					<div class="modal-body">
						<div class="container">
							<form action="{{url('/edit-todo')}}" method="POST" enctype="multipart/form-data">
								@csrf
								<div class="modal-body">
									<div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
										<input type="text" hidden value="{{$item->id}}">
										<label class="form-control-label" for="input-title">{{ __('Judul') }}</label>
										<input type="text" name="title" id="input-title" class="form-control form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}" placeholder="{{ __('Judul') }}" value="{{$item->title}}" autofocus>

										@if ($errors->has('title'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('title') }}</strong>
										</span>
										@endif
									</div>
									<div class="form-group{{ $errors->has('todo') ? ' has-danger' : '' }}">
										<label class="form-control-label" for="todo">{{ __('Todo Task') }}</label>
										<textarea id="todolist" name="todo">{{$item->list}}</textarea>
										@if ($errors->has('todo'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('todo') }}</strong>
										</span>
										@endif
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-secondary">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Hapus -->
		<div class="modal fade" id="modalDelete{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="modalDelete" aria-hidden="true">
			<div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
				<div class="modal-content bg-gradient-danger">
					<!-- <div class="modal-header">
						<h6 class="modal-title" id="modal-title-notification">Your attention is required</h6>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div> -->
					<form action="{{url('/delete-todo')}}" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="modal-body">
							<div class="py-3 text-center">
								<i class="ni ni-bell-55 ni-3x"></i>
								<h4 class="heading mt-4">Apakah anda yakin ingin menghapus task ini ?</h4>
							</div>
						</div>
						<div class="modal-footer">
							<input hidden id="id" name="id" type="text" value="{{$item->id}}">
							<button type="submit" class="btn btn-link text-white">Ya</a>
								<button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Tidak</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		@endforeach
	</div>
	@else
	<div class="row mt-5" style="justify-content: center;">
		<div class="col-xl-4">
			<div class="card shadow">
				<div class="card-header border-0">
					<div class="row align-items-center">
						<div class="col" style="text-align: center;">
							<h3 class="mb-0">No Record Found</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
	@include('layouts.footers.auth')
</div>
<script>
	$('#todolist').summernote({
		placeholder: 'To Do List',
		tabsize: 2,
		height: 120,
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'underline', 'clear']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']]
		]
	});
</script>
@endsection